Welcome to Solution Chiropractic, your natural solution to a pain-free and healthy lifestyle. As your downtown Seattle chiropractor, Dr. Nima specializes in corrective chiropractic care, using state-of-the-art technology and advanced approaches. Call +1(206) 453-2233 for more information.

Address: 1201 3rd Ave, #180, Seattle, WA 98101

Phone: 206-453-2233
